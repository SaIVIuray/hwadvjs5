class Card {
    constructor(post) {
      this.post = post;
      this.render();
    }
    
    render() {
      const cardElement = document.createElement('div');
      cardElement.classList.add('card');
      cardElement.id = `card-${this.post.id}`;

      cardElement.innerHTML = `
      <p> ${this.post.user.name}✪ ${this.post.user.email}</p>
        <h2>${this.post.title}.</h2>
        <p>${this.post.body}.</p>
        <button onclick="deletePost(${this.post.id})">Delete</button> 
        <button>✉ 928</button>
        <button>⮍ 361</button>
        <button>🤍 1483</button>`;

      this.element = cardElement;
    }
  }

  async function fetchData(url) {
    try {
      const response = await fetch(url);
      const data = await response.json();
      return data;
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  }

  function displayPosts(users, posts) {
    const twitterFeed = document.getElementById('twitter-feed');

    posts.forEach(post => {
      const user = users.find(u => u.id === post.userId);
      if (user) {
        const card = new Card({ ...post, user });
        twitterFeed.appendChild(card.element);
      }
    });
  }

  async function deletePost(postId) {
    const deleteUrl = `https://ajax.test-danit.com/api/json/posts/${postId}`;
    
    try {
      const response = await fetch(deleteUrl, {
        method: 'DELETE',
      });

      if (response.ok) {
        const cardElement = document.getElementById(`card-${postId}`);
        if (cardElement) {
          cardElement.remove();
        } else {
          console.error('Card element not found.');
        }
      } else {
        console.error('Error deleting post:', response.statusText);
      }
    } catch (error) {
      console.error('Error deleting post:', error);
    }
  }

  async function init() {
    const users = await fetchData('https://ajax.test-danit.com/api/json/users');
    const posts = await fetchData('https://ajax.test-danit.com/api/json/posts');

    if (users && posts) {
      displayPosts(users, posts);
    }
  }

  window.onload = init;